package ru.hostco.igor;

import java.io.*;
import org.apache.commons.cli.*;
import org.tmatesoft.svn.core.*;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

/**
 * Created by petetskih on 20.08.2014.
 */
public class SVNReader {
    public static void main(String[] args) throws SVNException, ParseException, IOException {

        CommandLineParser parser = new GnuParser();
        Options options = new Options();
        options.addOption("us", "svn-user", true, "user for svn");
        options.addOption("ps", "svn-pass", true, "pass for svn");
        options.addOption("ss", "svn-server", true, "url for svn repo");

        options.addOption("ur", "rb-user", true, "user for reviewboard");
        options.addOption("pr", "rb-pass", true, "pass for reviewboard");

        CommandLine cmd = parser.parse(options, args);
        System.out.print("command parameters was: ");
        for (String s : args) {
            System.out.print(s + " ");
        }
        System.out.println();
        if (args.length != 10) {
            HelpFormatter hf = new HelpFormatter();
            hf.printHelp("SVNReader", options);
            System.exit(-1);
        }

        String svnUser = cmd.getOptionValue("us");
        String svnPass = cmd.getOptionValue("ps");
        String svnServer = cmd.getOptionValue("ss");
        String rbUser = cmd.getOptionValue("ur");
        String rbPass = cmd.getOptionValue("pr");

        SVNURL url = SVNURL.parseURIEncoded(svnServer);
        SVNRepository repos = SVNRepositoryFactory.create(url);
        ISVNAuthenticationManager manager = SVNWCUtil.createDefaultAuthenticationManager(svnUser, svnPass);

        repos.setAuthenticationManager(manager);

        long headRevision = repos.getLatestRevision();
        SVNProperties props;
        props = repos.getRevisionProperties(headRevision, null);
        /**
         * need to add branches --branch BRANCH
         * neet to add numbers of fixed bugs --bugs-closed COMMASEPARATEDLIST
         */
        String rbt = "rbt post --publish --username " + rbUser+" --password " + rbPass +
                " --submit-as "+props.getStringValue("svn:author")+
                " --description \""+props.getStringValue("svn:log")+"\" "+
                " --summary \""+props.getStringValue("svn:log")+"\" "+headRevision;

        System.out.println(rbt);

        Process process = Runtime.getRuntime().exec(rbt);
        InputStream out = process.getInputStream();
        InputStream err = process.getErrorStream();

        BufferedReader reader = new BufferedReader(new InputStreamReader(out));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
        reader.close();

        BufferedReader errReader = new BufferedReader(new InputStreamReader(err));
        while ((line = errReader.readLine()) != null) {
            System.out.println(line);
        }
        errReader.close();
    }
}
